from django import forms

class TodoListForm(forms.Form):
    text=forms.CharField(max_length=45,
        widget = forms.TextInput(
            attrs={'class':'form-control','placeholder':'Enter todo eg Grocery Shopping','aria_label' :
            'Todo','aria-describedby':'add-btn'}))